###################################
## WORKSPACE SETUP AND STANDARD MODULE IMPORT
###################################

# standard python modules
import re # regular expressions
import imp
import inspect
import math
import numpy as np
import copy
import time
from datetime import datetime

# Directories and Solving
wd = os.path.dirname(os.path.realpath('__file__'))  # Getting working directory of Abaqus
os.chdir(wd)  # Change to this directory
ncpus = 2 # Number of cpus to use in simulations

# DEA umat file location
dea_model_file = os.environ['DEAMODEL_PATH']+'\\deamodel\\dea_subroutine.for'

###################################
## ABAQUS MODULE IMPORT AND SETUP
###################################

# abaqus modules
from part import *
from material import *
from section import *
from assembly import *
from step import *
from interaction import *
from load import *
from mesh import *
from optimization import *
from job import *
from sketch import *
from visualization import *
from connectorBehavior import *

from abaqus import *
from abaqusConstants import *

# allows us to use findat in journal file
session.journalOptions.setValues(replayGeometry=COORDINATE, recoverGeometry=COORDINATE)

###################################
## CUSTOM MODULE IMPORT
###################################
