# abaqus modules
from material import *

# constants defining ideal dielectric material obeying NH/gent models
class Hyperelastic(object):
    def __init__(
        self,
        name,                   # string describing the material
        mu,                     # shear modulus
        eps_rel=None,           # relative permittivity
        material_model='NH',    # material model type (Neo-Hookean = default)
        nu=0.49,                # Poisson's ratio, assumed to be close to .5 for hyperelastic
        abq_model=None          # abaqus model in which to generate material
    ):
        self.name = name
        self.mu = mu
        self.nu = nu
        if eps_rel is not None:
            self.eps = eps_rel*8.854e-9        # absolute permittivity, converted to mm-, mN-, V-consistent units

        # derived material constants
        self.C10 = self.mu/2.0                          # stiffness constant used for DEA mat input
        self.YM = self.mu*2*(1+self.nu)                 # Young's modulus ~= 3*mu
        self.kappa = self.YM/(3.0*(1.0-2.0*self.nu))    # bulk modulus >> mu
        self.D1 = 2.0/self.kappa                        # constant used for DEA mat input

        # TODO: Implement Gent model
        self.material_model = material_model

        if abq_model is not None:
            self.to_abaqus(abq_model)

    def to_abaqus(self, model):
        abq_mat = model.Material(name=self.name)
        # neo-hookean
        if self.material_model == 'NH':
            type = NEO_HOOKE
        abq_mat.Hyperelastic(
            materialType=ISOTROPIC, table=((self.C10, self.D1), ),
            testData=OFF, type=type, volumetricResponse=VOLUMETRIC_DATA)
        if hasattr(self, 'eps'):
            abq_mat.Conductivity(table=((self.eps, ),))
        self.abq_material = abq_mat
        return abq_mat



""" All linear materials, used as things like stiffeners and frames
"""

class LinearElastic(object):
    def __init__(
        self,
        name,    # name of the material, as a string
        YM,     # Young's Modulus - can make this a tuple if anisotropic
        nu      # Poisson's ratio
    ):
        self.YM = YM
        self.nu = nu
        self.name = name

    def to_abaqus(self, model):
        abq_mat = model.Material(name=self.name)
        abq_mat.Elastic(table=((self.YM, self.nu), ))
        self.abq_material = abq_mat
        return abq_mat
