"""Utility functions common to a variety of geometries and actuation conditions."""

# abaqus modules
from abaqus import *
from abaqusConstants import *

import numpy as np
import elements

import re
import os

class AbqModel(object):
    def __init__(
        self,
        model,      # the model(s) to construct the part in
        dea_model_file = os.path.join(os.environ['DEAMODEL_PATH'], 'deamodel', 'dea_subroutine.for') # string pointing to fortran file for DEA physics model
    ):
        self.mdb = mdb # we can get this from anywhere in Abaqus environment
        self.model = model
        self.dea_model_file = dea_model_file


    def set_up_job(self, job_name, permittivity):

        ncpu = 4
        part_phrase = '*Part, name='
        element_phrase = '*Element'
        startUelElemNum = 1000000 # avoid overlapping with abaqus-generated mechanical elements

        # TODO: generalize this to any kind of uel
        uel_definition, uel_properties = elements.brick_uel(permittivity)
        #--------------------------------------------------
        # 1. create job and input file
        #
        coupledINPName = job_name
        tempInpName = 'temp'
        # clear up any existing input files to avoid issues
        if os.path.exists(coupledINPName+'.inp'):
            os.remove(coupledINPName+'.inp')
        if os.path.exists(tempInpName+'.inp'):
            os.remove(tempInpName+'.inp')

        assem = self.model.rootAssembly
        assem.regenerate()
        mdb.Job(name=tempInpName, model=self.model.name)
        assem.regenerate()
        mdb.jobs[tempInpName].writeInput(consistencyChecking=OFF)
        #--------------------------------------------------
        # 2. read input file
        #
        # find the proper location to begin rewriting elements
        tempInp = open(tempInpName+'.inp', 'r')
        coupledInp = open(coupledINPName+'.inp', 'w')
        toggle1 = 0 		#=the first phrase is found
        toggle2 = 0 		#=the second phrase is not found

        # find where the desired elements are in the input file
        # TODO: search for specific element types.
        while toggle1==0 or toggle2==0: 	#=continue until the both phrases are found
            line = tempInp.readline()
            coupledInp.write(line)
            # if we are within a part
            if line.startswith(part_phrase):
                toggle1 = 1	#the first phrase is found
            # if we're within a part and have an element header
            if toggle1 == 1 and line.startswith(element_phrase):
                toggle2 = 1	#the second phrase is found

        # rewrite the elements
        n = 0 			#=number of elements
        elemUel=''
        elemLine = tempInp.readline()
        while elemLine.startswith('*')!=True:	#continue until next command starting with '*'
        	elem = re.findall('\d+', elemLine)
        	elem = [int(x) for x in elem]
        	coupledInp.write(elemLine)
        	elem[0] = elem[0] + startUelElemNum
        	elemUel = elemUel+',   '.join(map(str,elem))+'\n'
        	elemLine = tempInp.readline()

        coupledInp.write(uel_definition)
        coupledInp.write(elemUel)
        coupledInp.write(uel_properties)
        coupledInp.write(elemLine)
        lines = tempInp.read()
        coupledInp.write(lines)
        tempInp.close()
        coupledInp.close()
        # clean up the workspace a bit by deleting temp job
        del mdb.jobs[tempInpName]

        #--------------------------------------------------
        # 3. from input file read the mesh and create an input
        #    file for coupled electro-mechanical problem.
        #
        job = mdb.JobFromInputFile(name=job_name,
            inputFileName=coupledINPName + '.inp',
            userSubroutine=self.dea_model_file)
        job.setValues(numDomains=ncpu,
            activateLoadBalancing=False,
            numCpus=ncpu)

        return job
