import selection
import numpy as np
from abaqus import *
from abaqusConstants import *

def calc_layer_scaling(layer_height, distance, desired_num, active_ends = [False, False]):
    """ Find the necessary layer scaling to produce the desired (usually even)
    number of active DEA layers (meaning odd electrode layers)
    Distance includes inactive endcaps if present, but desired_num does not
    """
    # calculate the distance (thickness) over which the DEA is active
    active_distance = distance - layer_height*sum( (not x) for x in active_ends )
    # get the scaling factor
    simulated_height = active_distance/desired_num
    scaling = simulated_height/layer_height
    return scaling


def laminate(type, part, cells, starting_pt, direction, distance,
    layer_height, endcap_height,
    partition_ends = [False, False], active_ends = [False, False],
    name='cell', center_pt = None, model=None, solid_dir=None, solid_ref_pt=None):
    """Partition the selected cell into cartesian-oriented DEA layers and return
    sets containing the resulting cells and surfaces

    Keyword arguments:
    type - coordinate system to use ('cartesian' or 'polar')
    part - the abaqus part to partition
    cells - cell(s) within the part to partition
    starting_pt - point at which to start laminating. Avoid starting at the edge of a face - this could cause ambiguity with even and odd faces selection
    direction - electrode planes' normal vector (doesn't have to be unit magnitude)
    distance - distance over which to partition
    layer_height - the height/thickness of each layer
    endcap_height - the thickness of the endcaps on either end of the multilayer
    partition_ends - whether or not to partition at the endpts [START, END]
    active_ends - whether or not to treat the endcap layers as active layers
    name - the names of the sets to be returned ('[NAME]_odd_faces' and '[NAME]_even_faces')
    center_pt - the center point for a polar lamination - make sure this is away from the actual part you are cutting - intersections may mess up the lamination
    model - the model in the mdb
    """

    laminated_cells = part.Set(cells=cells, name='laminated_'+name)

    # convert directions to numpy vector if not already
    direction = np.array(direction)
    if solid_dir is not None:
        solid_dir = np.array(solid_dir)
        solid_dir = solid_dir/np.sqrt(np.dot(solid_dir, solid_dir))
    # normalize them to unit vectors
    normal = direction/np.sqrt(np.dot(direction, direction))
    n = 0 # electrode counter
    current_dist = 0
    even_pts = []
    odd_pts = []

    # calculate all the partitioning distances
    partition_start_dist = (not partition_ends[0])*endcap_height
    partition_end_dist = distance - (not partition_ends[1])*endcap_height

    partition_total_dist = partition_end_dist - partition_start_dist
    partition_num = int(round(partition_total_dist/layer_height, 6))+1 # we will always extend any layer at the end that is too small

    partition_dist = np.arange(partition_num-1)*layer_height + partition_start_dist
    partition_dist = np.append(partition_dist, partition_end_dist) # make sure the last one is exactly at the end

    # partition according to these distances

    if type == 'cartesian':
        for dist in partition_dist:
            pt = dist*normal + starting_pt
            partition_cell_by_point_and_normal(part, laminated_cells.cells, pt, normal)
    elif type == 'polar':
        # create a plane from point and normal
        lamination_axis = part.DatumAxisByTwoPoint( point1=center_pt, point2=tuple(np.array(center_pt)+normal) )
        partition_datum = part.DatumPlaneByPointNormal(normal=
            part.datums[lamination_axis.id], point=center_pt)
        rand_pts = rand_points_within_plane( center_pt, normal )
        up_axis = part.DatumAxisByTwoPoint( point1 = center_pt, point2 = tuple(rand_pts[1]) )
        for dist in partition_dist:
            radius = starting_pt + dist # starting point is a scalar value for polar coordinates
            # create a sketch centered at center_pt
            lamination_sketch = model.ConstrainedSketch(gridSpacing=5, name='sectioning_cylinder_profile',
                sheetSize=100, transform=part.MakeSketchTransform(part.datums[partition_datum.id],
                sketchPlaneSide=SIDE1,
                sketchUpEdge=part.datums[up_axis.id],
                sketchOrientation=RIGHT, origin=center_pt))
            lamination_sketch.CircleByCenterPerimeter(center=(0,0), point1=(radius, 0))
            # draw a guide cylinder
            part.SolidExtrude(depth=1.0,
                flipExtrudeDirection=ON, sketch=lamination_sketch, sketchOrientation=RIGHT,
                sketchPlane=part.datums[partition_datum.id], sketchPlaneSide=
                SIDE1, sketchUpEdge=part.datums[up_axis.id])
            # extend the cylinder's sides to create partition
            radial_vec = rand_pts[1] - np.array(center_pt)
            radial_vec = radial_vec/np.sqrt(np.dot(radial_vec, radial_vec))
            face_pt = np.array(center_pt) + radial_vec*radius - normal/2 # susbtract normal/2 to make sure we don't accidentally select the ends of the cylinder instead of its sides
            part.PartitionCellByExtendFace(cells=cells, extendFace=part.faces.findAt(
                tuple(face_pt), ))
        # now let's delete the cylinder solids we just made
        part.CutExtrude(depth=1.0, flipExtrudeDirection=OFF,
            sketch=lamination_sketch,
            sketchOrientation=RIGHT, sketchPlane=part.datums[partition_datum.id],
            sketchPlaneSide=SIDE1, sketchUpEdge=part.datums[up_axis.id])
    else:
        raise ValueError('Invalid type of lamination selected - cannot partition')

    # calculate all of the points to be used for applying voltage
    active_start_dist = (not active_ends[0])*endcap_height
    active_end_dist = distance - endcap_height*(not active_ends[1])
    active_num = int( round((active_end_dist-active_start_dist)/layer_height, 6) )+1
    active_dist = np.arange(active_num-1)*layer_height + active_start_dist
    active_dist = np.append(active_dist, active_end_dist)

    if type == 'cartesian':
        for i, dist in enumerate(active_dist):
            pt = dist*normal + starting_pt
            if i % 2 == 0:
                even_pts.append(pt)
            else:
                odd_pts.append(pt)
        if len(active_dist) % 2 == 0:
             print('WARNING: Even number of electrode layers (odd number of active layers) in "%s" - may cause convergence issues' % name)

    elif type == 'polar':
        if solid_dir is None:
            solid_dir = up_axis
        solid_axis = part.DatumAxisByTwoPoint( point1=solid_ref_pt, point2=tuple(np.array(solid_ref_pt)+solid_dir) )
        for i, dist in enumerate(active_dist):
            pt = (dist+starting_pt)*solid_dir + np.array(solid_ref_pt)
            if i % 2 == 0:
                even_pts.append(pt)
            else:
                odd_pts.append(pt)
        if len(active_dist) % 2 == 0:
             print('WARNING: Even number of electrode layers (odd number of active layers) in "%s" - may cause convergence issues' % name)

    # TODO: make the selection only valid for faces with proper angles
    odd_faces = selection.group_faces(part, odd_pts, name+'_odd_faces')
    even_faces = selection.group_faces(part, even_pts, name+'_even_faces')

    return odd_faces, even_faces, laminated_cells

def partition_cell_by_point_and_normal(part, cells, point, normal):
    rand_pts = rand_points_within_plane(point, normal)
    part.PartitionCellByPlaneThreePoints( cells=cells,
        point1=point, point2=rand_pts[0], point3=rand_pts[1] )

def rand_points_within_plane(point, normal):
    ''' Returns two random points within a plane defined by a point and normal
    '''
    point = np.array(point)
    normal = np.array(normal)
    # get constant in plane equation Ax + By + Cz = D
    D = np.dot(point, normal)

    # get perpendicular vector to the normal
    rand_vec = np.random.rand(3)+1 # make sure its magnitude is nonzero
    perp_vec = np.cross(normal, rand_vec)
    # get a mutually orthogonal vector
    mutual_perp = np.cross(normal, perp_vec)

    points = np.empty([2, 3])
    # create two random points by adding each vector to the origin
    points[0] = point + perp_vec
    points[1] = point + mutual_perp

    return points

class Section(object):
    def __init__(
        self,
        name,                                       # name for the section
        material,                                   # material of the section
        geometry_set = None,                        # abaqus geometry to assign the section to
        part = None,                                # abaqus part to assign to
        model = None,                               # abq model to work in
        prestretch = (None, None, None),    # prestretching of elastic
        prestress = (None, None, None)      # alternate prestretch method
    ):
        self.name = name
        self.material = material
        self.part = part
        # create the section in the abaqus model if we can
        if model is not None:
            self.model = model
            self.to_abaqus(model)
        # assign the section to the proper geometry set if we can
        if geometry_set is not None:
            self.geometry_set = geometry_set
            self.assign(part, geometry_set)
        # if we applied any prestretch, then assign it
        if prestretch.count(None) < 3:
            self.prestretch = prestretch
        if prestress.count(None) < 3:
            self.prestress = prestress

    def assign(self, part, geometry_set):
        """ Assign the section to the desired geometry """
        part.SectionAssignment(offset=0.0, offsetField='', offsetType=MIDDLE_SURFACE,
            region=geometry_set, sectionName=self.name, thicknessAssignment=FROM_SECTION)
        self.part = part
        self.geometry_set = geometry_set

    def to_abaqus(self, model):
        """ Create the section in Abaqus """
        self.model.HomogeneousSolidSection(material=self.material.name,
            name=self.name, thickness=None)
