def brick_uel(permittivity, density=0., quadrature_points=8): # TODO: put permittivity and other arguments
    # UEL element definition: *** DEFINE THE NUMBER OF NODES, COORDINATES AND ELEMENT DOFs ***
    # 2D:
    #    coordinates = 2 => line: 1, 2, 11
    #    nodes = 3 (linear tri), 4 (linear quad), 6 (quadratic tri), 8 (quadratic quad)
    # 3D:
    #    coordinates = 3 => line: 1, 2, 3, 11
    #    nodes = 8 (linear brick)
    uel_definition = ('**\n'+
        '**\n'+
        '**\n'+
        '*user element, nodes = 8, coordinates = 3, type = U1, \n'+
        'properties = 2, i properties = 1, variables = 1, unsymm\n'+
        '1, 2, 3, 11\n'+
        '*element, type = U1, elset = uels\n')
     # UEL properties: DEFINE PERMITTIVITY AND THE NUMBER OF GAUSSIAN QUADRATURE POINTS
    uel_properties = ('*uel property, elset = uels\n'+
        str(permittivity)+', '+ str(density) + ', ' + str(quadrature_points) +
        '\n**\n'+
        '**\n' +
        '**\n')
    return uel_definition, uel_properties
