      subroutine uel(rhs,amatrx,svars,energy,ndofel,nrhs,nsvars,
     +     props,nprops,coords,mcrd,nnode,u,du,v,accn,jtype,
     +     time,dtime,kstep,kinc,jelem,params,ndload,jdltyp,adlmag,
     +     predef,npredf,lflags,mlvarx,ddlmag,mdload,pnewdt,jprops,
     +     njprop,period)

      implicit none
      
      ! variable that have to be defined in the uel
      real*8 rhs(mlvarx,*),amatrx(ndofel,ndofel),svars(*),energy(8),
     +  pnewdt
      
      ! variables tha are passed into the uel for information
      integer ndofel,nrhs,nsvars,nprops,mcrd,nnode,jtype,kstep,kinc,
     +  jelem,ndload,jdltyp(mdload,*),npredf,lflags(*),mlvarx,mdload,
     +  jprops(*),njprop
      real*8 props(*),coords(mcrd,nnode),u(ndofel),du(mlvarx,*),
     +  vel(ndofel),accn(ndofel),time(2),dtime,params(*),
     +  adlmag(mdload,*),predef(2,npredf,nnode),ddlmag(mdload,*),period
      
      ! variables that are used locally inside the uel
      integer i,j,k,l,knode,knode1,knode2,kgauss,ngauss
      real*8 epsilon,x(nnode,mcrd),phi(nnode),ident(mcrd,mcrd),
     +       sh(nnode,jprops(1)),dshdxi(nnode,mcrd,jprops(1)),
     +       xi(mcrd,jprops(1)),wgauss(jprops(1)),
     +       dshdx(nnode,mcrd,jprops(1)),
     +       dxdxi(mcrd,mcrd,jprops(1)),dxdxiDet(jprops(1)),
     +       e(mcrd,jprops(1)),ekek(jprops(1)),
     +       Tmax(mcrd,mcrd,jprops(1)),res(nnode,mcrd),
     +       dTmaxdF(mcrd,mcrd,mcrd,mcrd,jprops(1)),
     +       kuu(nnode,nnode,mcrd,mcrd),
     +       dTmaxdE(mcrd,mcrd,mcrd,jprops(1)),
     +       kuphi(nnode,nnode,mcrd),
     +       dDdF(mcrd,mcrd,mcrd,jprops(1)),kphiu(nnode,nnode,mcrd),
     +       rho0,beta,gamma,x0(nnode,mcrd),v0(mcrd,jprops(1)),
     +       acc0(mcrd,jprops(1)),acc(mcrd,jprops(1)),
     +       v(mcrd,jprops(1)),f(mcrd,mcrd,jprops(1)),
     +       fInv(mcrd,mcrd,jprops(1)),fDet(jprops(1)),rho(jprops(1))
      
      ! get the permittivity, number of Gaussian quadrature points, 
      ! nodal position and electric potential at each node.
      epsilon = props(1)
      ngauss = jprops(1)
      k = 0
      do knode = 1, nnode
         do i = 1, mcrd
            k = k+1
            x(knode,i) = coords(i,knode) + u(k)
         enddo
         k = k+1
         phi(knode) = u(k)
      enddo
      ! identity matrix
      ident = 0.d0
      do i = 1, mcrd
         ident(i,i) = 1.d0
      enddo
      
      ! calculate the shape function and its derivatives in local coords
      call ShapeFunction(sh,dshdxi,xi,wgauss,nnode,mcrd,ngauss)
      
      ! map the derivatives to global coords
      call Local2Global(dshdx,dxdxi,dxdxiDet,dshdxi,f,fInv,fDet,
     +                        x,coords,nnode,mcrd,ngauss)
      
      ! calcualte the electric potential at each Gauss point
      do kgauss = 1, ngauss
         do i = 1, mcrd
            e(i,kgauss) = 0.d0
            do knode = 1, nnode
               e(i,kgauss)=e(i,kgauss)-dshdx(knode,i,kgauss)*phi(knode)
            enddo
         enddo
      enddo
      
      ! calcualte the Maxwell stress at each Gauss point
      do kgauss = 1, ngauss
         ekek(kgauss) = 0.d0
         do i = 1, mcrd
            ekek(kgauss) = ekek(kgauss) + e(i,kgauss)*e(i,kgauss)
         enddo
         do i = 1, mcrd
            do j = 1, mcrd
               Tmax(i,j,kgauss) = epsilon*(e(i,kgauss)*e(j,kgauss)
     +                            -0.5d0 * ekek(kgauss)*ident(i,j))
            enddo
         enddo
      enddo
      
      ! calcualte the residual integral 
      do knode = 1, nnode
         do i = 1, mcrd
            res(knode,i) = 0.d0
            do kgauss = 1, ngauss
               do j = 1, mcrd
                  res(knode,i) = res(knode,i) - Tmax(i,j,kgauss)*
     +                           dshdx(knode,j,kgauss)*wgauss(kgauss)*
     +                           dxdxiDet(kgauss)
               enddo
            enddo
         enddo
      enddo
      
      ! output the residual vector
      k = 0
      do knode = 1, nnode
         do i = 1, mcrd
            k = k+1
            rhs(k,1) = res(knode,i)
         enddo
         k = k+1
      enddo
      
      ! calcualte the Kuu stiffness matrix
      do kgauss = 1, ngauss
       do i = 1, mcrd
        do j = 1, mcrd
         do k = 1, mcrd
          do l = 1, mcrd
           dTmaxdF(i,j,k,l,kgauss) = epsilon*
     +                       (-ident(i,l)*e(j,kgauss)*e(k,kgauss)
     +                        -ident(j,k)*e(i,kgauss)*e(l,kgauss)
     +                        -ident(j,l)*e(i,kgauss)*e(k,kgauss)
     +                        +ident(i,j)*e(k,kgauss)*e(l,kgauss)
     +                        +ident(k,l)*e(i,kgauss)*e(j,kgauss)
     +                        +0.5d0*ekek(kgauss)*ident(i,l)*ident(j,k)
     +                        -0.5d0*ekek(kgauss)*ident(i,j)*ident(k,l))
          enddo
         enddo
        enddo
       enddo
      enddo
      do knode1 = 1, nnode
       do knode2 = 1, nnode
        do i = 1, mcrd
         do k = 1, mcrd
          kuu(knode1,knode2,i,k) = 0.d0
          do kgauss = 1, ngauss
           do j = 1, mcrd
            do l = 1, mcrd
             kuu(knode1,knode2,i,k) = kuu(knode1,knode2,i,k) + 
     +         dshdx(knode1,j,kgauss)*dTmaxdF(i,j,k,l,kgauss)*
     +         dshdx(knode2,l,kgauss)*wgauss(kgauss)*dxdxiDet(kgauss)
            enddo
           enddo
          enddo
         enddo
        enddo
       enddo
      enddo
      
      ! calcualte the kuphi stiffness matrix
      do kgauss = 1, ngauss
       do i = 1, mcrd
        do j = 1, mcrd
         do l = 1, mcrd
          dTmaxdE(i,j,l,kgauss) = epsilon*(ident(i,l)*e(j,kgauss)
     +                   +ident(j,l)*e(i,kgauss)-ident(i,j)*e(l,kgauss))
         enddo
        enddo
       enddo
      enddo
      do knode1 = 1, nnode
       do knode2 = 1, nnode
        do i = 1, mcrd
         kuphi(knode1,knode2,i) = 0.d0
         do kgauss = 1, ngauss
          do j = 1, mcrd
           do l = 1, mcrd
            kuphi(knode1,knode2,i) = kuphi(knode1,knode2,i) - 
     +         dshdx(knode1,j,kgauss)*dTmaxdE(i,j,l,kgauss)*
     +         dshdx(knode2,l,kgauss)*wgauss(kgauss)*dxdxiDet(kgauss)
           enddo
          enddo
         enddo
        enddo
       enddo
      enddo
      
      ! calcualte the kphiu stiffness matrix
      do kgauss = 1, ngauss
        do j = 1, mcrd
          do k = 1, mcrd
            do l = 1, mcrd
              dDdF(j,k,l,kgauss) = -epsilon*(ident(j,k)*e(l,kgauss)
     +                   +ident(j,l)*e(k,kgauss)-ident(k,l)*e(j,kgauss))
            enddo
          enddo
        enddo
      enddo
      do knode1 = 1, nnode
        do knode2 = 1, nnode
          do k = 1, mcrd
          kphiu(knode1,knode2,k) = 0.d0
            do kgauss = 1, ngauss
              do j = 1, mcrd
                do l = 1, mcrd
                  kphiu(knode1,knode2,k) = kphiu(knode1,knode2,k) - 
     +                     dshdx(knode1,j,kgauss)*dDdF(j,k,l,kgauss)*
     +                     dshdx(knode2,l,kgauss)*wgauss(kgauss)*
     +                     dxdxiDet(kgauss)
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
      
      ! output the stiffness matrix
      k = 0
      do knode1 = 1, nnode
        do i = 1, mcrd
          k = k+1
          l = 0
          do knode2 = 1, nnode
            do j = 1, mcrd
              l = l+1
              amatrx(k,l) = kuu(knode1,knode2,i,j)
            enddo
            l = l+1
            amatrx(k,l) = kuphi(knode1,knode2,i)
          enddo
        enddo
        k = k+1
        l = 0
        do knode2 = 1, nnode
          do j = 1, mcrd
            l = l+1
            amatrx(k,l) = kphiu(knode1,knode2,j)
          enddo
          l = l+1
        enddo
      enddo
      
      return
      endsubroutine uel
      
c---------------------------------------------------------------c
c                                                               c
c    ShapeFunction subroutine calculates the shape functions    c
c    and its derivatives in the local coordiantes system.       c
c                                                               c
c---------------------------------------------------------------c
      subroutine ShapeFunction(sh,dshdxi,xi,wgauss,nnode,mcrd,ngauss)
      implicit none
      ! input variables
      integer nnode,mcrd,ngauss
      ! output variables
      real*8 sh(nnode,ngauss),dshdxi(nnode,mcrd,ngauss),xi(mcrd,ngauss),
     +       wgauss(ngauss)
      ! local variables
      integer kgauss
      real*8 f,g,h,r
      
      if(mcrd.eq.2) then
      !-------------!
      ! 2D elements !
      !-------------!
         if(nnode.eq.3.or.nnode.eq.6) then
         !------------------------!
         ! 2D triangular elements !
         !------------------------!
            ! Gaussian quadrature points and wights for a 2D triangular element
            if(ngauss.eq.1) then
               xi(1,1) = 1.d0/3.d0
               xi(2,1) = 1.d0/3.d0
               wgauss = 0.5d0
            elseif(ngauss.eq.3) then
               xi(1,1) = 1.d0/6.d0
               xi(2,1) = 1.d0/6.d0
               xi(1,2) = 2.d0/3.d0
               xi(2,2) = 1.d0/6.d0
               xi(1,3) = 1.d0/6.d0
               xi(2,3) = 2.d0/3.d0
               wgauss = 1.d0/3.d0
            else
               ! error: incorrect number of Gaussian quadrature points
               write(*,*) 'For 2D triangular elements the number of ',
     +                 'Gaussian quadrature points must be either ',
     +                 '1 or 3.'
               call xit
            endif
            if(nnode.eq.3) then
            !------------------------------!
            ! 2D lienar triangular element !
            !------------------------------!
               do kgauss = 1, ngauss
                  g = xi(1,kgauss)
                  h = xi(2,kgauss)
                  sh(1,kgauss) = 1-g-h
                  sh(2,kgauss) = g
                  sh(3,kgauss) = h
                  dshdxi(1,1,kgauss) = -1.d0
                  dshdxi(1,2,kgauss) = -1.d0
                  dshdxi(2,1,kgauss) = 1.d0
                  dshdxi(2,2,kgauss) = 0.d0
                  dshdxi(3,1,kgauss) = 0.d0
                  dshdxi(3,2,kgauss) = 1.d0
               enddo
            elseif(nnode.eq.6) then
            !---------------------------------!
            ! 2D quadratic triangular element !
            !---------------------------------!
               do kgauss = 1, ngauss
                  g = xi(1,kgauss)
                  h = xi(2,kgauss)
                  sh(1,kgauss) = 2.d0*(0.5d0-g-h)*(1.d0-g-h)
                  sh(2,kgauss) = 2.d0*g*(g-0.5d0)
                  sh(3,kgauss) = 2.d0*h*(h-0.5d0)
                  sh(4,kgauss) = 4.d0*g*(1.d0-g-h)
                  sh(5,kgauss) = 4.d0*g*h
                  sh(6,kgauss) = 4.d0*h*(1.d0-g-h)
                  dshdxi(1,1,kgauss) = -3.d0+4.d0*(g+h)
                  dshdxi(1,2,kgauss) = -3.d0+4.d0*(g+h)
                  dshdxi(2,1,kgauss) = 4.d0*g-1.d0
                  dshdxi(2,2,kgauss) = 0.d0
                  dshdxi(3,1,kgauss) = 0.d0
                  dshdxi(3,2,kgauss) = 4.d0*h-1.d0
                  dshdxi(4,1,kgauss) = 4.d0*(1.d0-2.d0*g-h)
                  dshdxi(4,2,kgauss) = -4.d0*g
                  dshdxi(5,1,kgauss) = +4.d0*h
                  dshdxi(5,2,kgauss) = +4.d0*g
                  dshdxi(6,1,kgauss) = -4.d0*h
                  dshdxi(6,2,kgauss) = 4.d0*(1-g-2.d0*h)
               enddo
            endif
         elseif(nnode.eq.4.or.nnode.eq.8) then
         !---------------------------!
         ! 2D quadrilateral elements !
         !---------------------------!
         ! Gaussian quadrature points and wights for a 2D rectangular element
            if(ngauss.eq.1) then
               xi(1,1) = 0.d0
               xi(2,1) = 0.d0
               wgauss = 2.d0
            elseif(ngauss.eq.4) then
               f = dsqrt(1.d0/3.d0)
               xi(1,1) = -f
               xi(2,1) = -f
               xi(1,2) =  f
               xi(2,2) = -f
               xi(1,3) = -f
               xi(2,3) =  f
               xi(1,4) =  f
               xi(2,4) =  f
               wgauss = 1.d0
            elseif(ngauss.eq.9) then
               f = dsqrt(3.d0/5.d0)
               xi(1,1) = -f
               xi(2,1) = -f
               xi(1,2) = 0.d0
               xi(2,2) = -f
               xi(1,3) = f
               xi(2,3) = -f
               xi(1,4) = -f
               xi(2,4) = 0.d0
               xi(1,5) = 0.d0
               xi(2,5) = 0.d0
               xi(1,6) = f
               xi(2,6) = 0.d0
               xi(1,7) = -f
               xi(2,7) = f
               xi(1,8) = 0.d0
               xi(2,8) = f
               xi(1,9) = f
               xi(2,9) = f
               wgauss(1) = 25.d0/81.d0
               wgauss(2) = 40.d0/81.d0
               wgauss(3) = 25.d0/81.d0
               wgauss(4) = 40.d0/81.d0
               wgauss(5) = 64.d0/81.d0
               wgauss(6) = 40.d0/81.d0
               wgauss(7) = 25.d0/81.d0
               wgauss(8) = 40.d0/81.d0
               wgauss(9) = 25.d0/81.d0
            else
               ! error: incorrect number of Gaussian quadrature points
               write(*,*) 'For 2D quadrilateral elements the number ',
     +                 'of Gaussian quadrature points must be either ',
     +                 '1, 4, or 9.'
               call xit
            endif
            if(nnode.eq.4) then
            !---------------------------------!
            ! 2D lienar quadrilateral element !
            !---------------------------------!
               do kgauss = 1, ngauss
                  g = xi(1,kgauss)
                  h = xi(2,kgauss)
                  sh(1,kgauss) = 0.25d0*(1.d0-g)*(1.d0-h)
                  sh(2,kgauss) = 0.25d0*(1.d0+g)*(1.d0-h)
                  sh(3,kgauss) = 0.25d0*(1.d0+g)*(1.d0+h)
                  sh(4,kgauss) = 0.25d0*(1.d0-g)*(1.d0+h)
                  dshdxi(1,1,kgauss) = -0.25d0*(1.d0-h)
                  dshdxi(1,2,kgauss) = -0.25d0*(1.d0-g)
                  dshdxi(2,1,kgauss) =  0.25d0*(1.d0-h)
                  dshdxi(2,2,kgauss) = -0.25d0*(1.d0+g)
                  dshdxi(3,1,kgauss) =  0.25d0*(1.d0+h)
                  dshdxi(3,2,kgauss) =  0.25d0*(1.d0+g)
                  dshdxi(4,1,kgauss) = -0.25d0*(1.d0+h)
                  dshdxi(4,2,kgauss) =  0.25d0*(1.d0-g)
               enddo
            elseif(nnode.eq.8) then
            !------------------------------------!
            ! 2D quadratic quadrilateral element !
            !------------------------------------!
               do kgauss = 1, ngauss
                  g = xi(1,kgauss)
                  h = xi(2,kgauss)
                  sh(1,kgauss) = -0.25d0*(1.d0-g)*(1.d0-h)*(1.d0+g+h)
                  sh(2,kgauss) = -0.25d0*(1.d0+g)*(1.d0-h)*(1.d0-g+h)
                  sh(3,kgauss) = -0.25d0*(1.d0+g)*(1.d0+h)*(1.d0-g-h)
                  sh(4,kgauss) = -0.25d0*(1.d0-g)*(1.d0+h)*(1.d0+g-h)
                  sh(5,kgauss) = 0.5d0*(1.d0-g)*(1.d0+g)*(1.d0-h)
                  sh(6,kgauss) = 0.5d0*(1.d0-h)*(1.d0+h)*(1.d0+g)
                  sh(7,kgauss) = 0.5d0*(1.d0-g)*(1.d0+g)*(1.d0+h)
                  sh(8,kgauss) = 0.5d0*(1.d0-h)*(1.d0+h)*(1.d0-g)
                  dshdxi(1,1,kgauss) = -0.25d0*(-(1.d0-h)*(1.d0+g+h)+
     +                                 (1.d0-g)*(1.d0-h))
                  dshdxi(1,2,kgauss) = -0.25d0*(-(1.d0-g)*(1.d0+g+h)+
     +                                  (1.d0-g)*(1.d0-h))
                  dshdxi(2,1,kgauss) = -0.25d0*((1.d0-h)*(1.d0-g+h)-
     +                                  (1.d0+g)*(1.d0-h))
                  dshdxi(2,2,kgauss) = -0.25d0*(-(1.d0+g)*(1.d0-g+h)+
     +                                  (1.d0+g)*(1.d0-h))
                  dshdxi(3,1,kgauss) = -0.25d0*((1.d0+h)*(1.d0-g-h)-
     +                                  (1.d0+g)*(1.d0+h))
                  dshdxi(3,2,kgauss) = -0.25d0*((1.d0+g)*(1.d0-g-h)-
     +                                  (1.d0+g)*(1.d0+h))
                  dshdxi(4,1,kgauss) = -0.25d0*(-(1.d0+h)*(1.d0+g-h)+
     +                                  (1.d0-g)*(1.d0+h))
                  dshdxi(4,2,kgauss) = -0.25d0*((1.d0-g)*(1.d0+g-h)-
     +                                  (1.d0-g)*(1.d0+h))
                  dshdxi(5,1,kgauss) = -g*(1.d0-h)
                  dshdxi(5,2,kgauss) = -0.5d0*(1.d0-g)*(1.d0+g)
                  dshdxi(6,1,kgauss) = 0.5d0*(1.d0-h)*(1.d0+h)
                  dshdxi(6,2,kgauss) = -h*(1.d0+g)
                  dshdxi(7,1,kgauss) = -g*(1.d0+h)
                  dshdxi(7,2,kgauss) = 0.5d0*(1.d0-g)*(1.d0+g)
                  dshdxi(8,1,kgauss) = -0.5d0*(1.d0-h)*(1.d0+h)
                  dshdxi(8,2,kgauss) = -h*(1.d0-g)
               enddo
            endif
         else
         !-----------------------------!
         ! error: undefined 2D element !
         !-----------------------------!
            write(*,*) 'For 2D elements the number of nodes must be ',
     +              'either 3 (lienar tri), 6 (quadratic tri), ',
     +              '4 (linear quad), or 8 (quadratic quad).'
            call xit
         endif
      elseif(mcrd.eq.3) then
      !-------------!
      ! 3D elements !
      !-------------!
         if(nnode.eq.8) then
            !-------------------------!
            ! 3D linear break element !
            !-------------------------!
            if(ngauss.eq.1) then
               xi(1,1) = 0.d0
               xi(2,1) = 0.d0
               xi(3,1) = 0.d0
               wgauss = 8.d0
            elseif(ngauss.eq.8) then
               f = dsqrt(1.d0/3.d0)
               xi(1,1) = -f
               xi(2,1) = -f
               xi(3,1) = -f
               xi(1,2) = +f
               xi(2,2) = -f
               xi(3,2) = -f
               xi(1,3) = -f
               xi(2,3) = +f
               xi(3,3) = -f
               xi(1,4) = +f
               xi(2,4) = +f
               xi(3,4) = -f
               xi(1,5) = -f
               xi(2,5) = -f
               xi(3,5) = +f
               xi(1,6) = +f
               xi(2,6) = -f
               xi(3,6) = +f
               xi(1,7) = -f
               xi(2,7) = +f
               xi(3,7) = +f
               xi(1,8) = +f
               xi(2,8) = +f
               xi(3,8) = +f
               wgauss = 1.d0
            else
               write(*,*) 'Error: number of Gaussian quadrature points',
     +                    ' for 3D elements must be either 1 or 8.'
               call xit
            endif
            do kgauss = 1, ngauss
               g = xi(1,kgauss)
               h = xi(2,kgauss)
               r = xi(3,kgauss)
               sh(1,kgauss) = (1.d0-g)*(1.d0-h)*(1.d0-r)/8.d0
               sh(2,kgauss) = (1.d0+g)*(1.d0-h)*(1.d0-r)/8.d0
               sh(3,kgauss) = (1.d0+g)*(1.d0+h)*(1.d0-r)/8.d0
               sh(4,kgauss) = (1.d0-g)*(1.d0+h)*(1.d0-r)/8.d0
               sh(5,kgauss) = (1.d0-g)*(1.d0-h)*(1.d0+r)/8.d0
               sh(6,kgauss) = (1.d0+g)*(1.d0-h)*(1.d0+r)/8.d0
               sh(7,kgauss) = (1.d0+g)*(1.d0+h)*(1.d0+r)/8.d0
               sh(8,kgauss) = (1.d0-g)*(1.d0+h)*(1.d0+r)/8.d0
               dshdxi(1,1,kgauss) = -(1.d0-h)*(1.d0-r)/8.d0
               dshdxi(1,2,kgauss) = -(1.d0-g)*(1.d0-r)/8.d0
               dshdxi(1,3,kgauss) = -(1.d0-g)*(1.d0-h)/8.d0
               dshdxi(2,1,kgauss) = +(1.d0-h)*(1.d0-r)/8.d0
               dshdxi(2,2,kgauss) = -(1.d0+g)*(1.d0-r)/8.d0
               dshdxi(2,3,kgauss) = -(1.d0+g)*(1.d0-h)/8.d0
               dshdxi(3,1,kgauss) = +(1.d0+h)*(1.d0-r)/8.d0
               dshdxi(3,2,kgauss) = +(1.d0+g)*(1.d0-r)/8.d0
               dshdxi(3,3,kgauss) = -(1.d0+g)*(1.d0+h)/8.d0
               dshdxi(4,1,kgauss) = -(1.d0+h)*(1.d0-r)/8.d0
               dshdxi(4,2,kgauss) = +(1.d0-g)*(1.d0-r)/8.d0
               dshdxi(4,3,kgauss) = -(1.d0-g)*(1.d0+h)/8.d0
               dshdxi(5,1,kgauss) = -(1.d0-h)*(1.d0+r)/8.d0
               dshdxi(5,2,kgauss) = -(1.d0-g)*(1.d0+r)/8.d0
               dshdxi(5,3,kgauss) = +(1.d0-g)*(1.d0-h)/8.d0
               dshdxi(6,1,kgauss) = +(1.d0-h)*(1.d0+r)/8.d0
               dshdxi(6,2,kgauss) = -(1.d0+g)*(1.d0+r)/8.d0
               dshdxi(6,3,kgauss) = +(1.d0+g)*(1.d0-h)/8.d0
               dshdxi(7,1,kgauss) = +(1.d0+h)*(1.d0+r)/8.d0
               dshdxi(7,2,kgauss) = +(1.d0+g)*(1.d0+r)/8.d0
               dshdxi(7,3,kgauss) = +(1.d0+g)*(1.d0+h)/8.d0
               dshdxi(8,1,kgauss) = -(1.d0+h)*(1.d0+r)/8.d0
               dshdxi(8,2,kgauss) = +(1.d0-g)*(1.d0+r)/8.d0
               dshdxi(8,3,kgauss) = +(1.d0-g)*(1.d0+h)/8.d0
            enddo
         elseif (nnode.eq.6) then
            !------------------!
            ! 3D wedge element !
            !------------------!
            if (ngauss.eq.6) then
               xi(1,1) = 1.d0/6.d0
               xi(2,1) = 1.d0/6.d0
               xi(3,1) = dsqrt(1.d0/3.d0)
               xi(1,2) = 1.d0/6.d0
               xi(2,2) = 1.d0/6.d0
               xi(3,2) = -dsqrt(1.d0/3.d0)
               xi(1,3) = 1.d0/6.d0
               xi(2,3) = 4.d0/6.d0
               xi(3,3) = dsqrt(1.d0/3.d0)
               xi(1,4) = 1.d0/6.d0
               xi(2,4) = 4.d0/6.d0
               xi(3,4) = -dsqrt(1.d0/3.d0)
               xi(1,5) = 4.d0/6.d0
               xi(2,5) = 1.d0/6.d0
               xi(3,5) = dsqrt(1.d0/3.d0)
               xi(1,6) = 4.d0/6.d0
               xi(2,6) = 1.d0/6.d0
               xi(3,6) = -dsqrt(1.d0/3.d0)
               wgauss = 1.d0/6.d0
            else
               write(*,*) 'Error: number of Gaussian quadrature points',
     +                    ' for wedge element must be either 1 or 6.'
               call xit
            endif
            do kgauss = 1, ngauss
               g = xi(1,kgauss)
               h = xi(2,kgauss)
               r = xi(3,kgauss)
               sh(1,kgauss) = 0.5d0*(1.d0-r)*(1.d0-g-h)
               sh(2,kgauss) = 0.5d0*(1.d0-r)*g
               sh(3,kgauss) = 0.5d0*(1.d0-r)*h
               sh(4,kgauss) = 0.5d0*(1.d0+r)*(1.d0-g-h)
               sh(5,kgauss) = 0.5d0*(1.d0+r)*g
               sh(6,kgauss) = 0.5d0*(1.d0+r)*h
               dshdxi(1,1,kgauss) = -0.5d0*(1.d0-r)
               dshdxi(1,2,kgauss) = -0.5d0*(1.d0-r)
               dshdxi(1,3,kgauss) = -0.5d0*(1.d0-g-h)
               dshdxi(2,1,kgauss) = 0.5d0*(1.d0-r)
               dshdxi(2,2,kgauss) = 0.d0
               dshdxi(2,3,kgauss) = -0.5d0*g
               dshdxi(3,1,kgauss) = 0.d0
               dshdxi(3,2,kgauss) = 0.5d0*(1.d0-r)
               dshdxi(3,3,kgauss) = -0.5d0*h
               dshdxi(4,1,kgauss) = -0.5d0*(1.d0+r)
               dshdxi(4,2,kgauss) = -0.5d0*(1.d0+r)
               dshdxi(4,3,kgauss) = 0.5d0*(1.d0-g-h)
               dshdxi(5,1,kgauss) = 0.5d0*(1.d0+r)
               dshdxi(5,2,kgauss) = 0.0d0
               dshdxi(5,3,kgauss) = 0.5d0*g
               dshdxi(6,1,kgauss) = 0.d0
               dshdxi(6,2,kgauss) = 0.5d0*(1.d0+r)
               dshdxi(6,3,kgauss) = 0.5d0*h
            enddo
         else
         !-----------------------------!
         ! error: undefined 3D element !
         !-----------------------------!
            write(*,*) 'Error: This 3D element is not defined yet.'
            call xit
         endif
      else
         ! error: neither 2D nor 3D
         write(*,*) 'MCRD (dimensions) must be either 2 or 3.'
         call xit
      endif
      return
      endsubroutine ShapeFunction

c---------------------------------------------------------------c
c                                                               c
c    Local2Global calculates the Jacobian matrix, its           c
c    derivatives, and the derivatives of the shape functions    c
c    with respect to the global Cartesian coordiantes.          c
c                                                               c
c---------------------------------------------------------------c
      subroutine Local2Global(dshdx,dxdxi,dxdxiDet,dshdxi,f,fInv,fDet,
     +                        x,coords,nnode,mcrd,ngauss)
      implicit none
      ! input variables
      integer nnode,mcrd,ngauss
      real*8 dshdxi(nnode,mcrd,ngauss),x(nnode,mcrd),coords(mcrd,nnode)
      ! output variables
      real*8 dshdx(nnode,mcrd,ngauss),dxdxi(mcrd,mcrd,ngauss),
     +       dxdxiDet(ngauss),f(mcrd,mcrd,ngauss),
     +       fInv(mcrd,mcrd,ngauss),fDet(ngauss),fInvDet(ngauss)
      ! local variables
      integer knode,kgauss,i,j
      real*8 dxdxiInv(mcrd,mcrd,ngauss),m(mcrd,mcrd),mInv(mcrd,mcrd),
     +       mDet
      
      ! calcualte the Jacobian matrix (iso-parametric)
      do kgauss = 1, ngauss
         do i = 1, mcrd
            do j = 1, mcrd
               dxdxi(i,j,kgauss) = 0.d0
               do knode = 1, nnode
                  dxdxi(i,j,kgauss) = dxdxi(i,j,kgauss) + 
     +                              dshdxi(knode,j,kgauss)*x(knode,i)
               enddo
            enddo
         enddo
      enddo
      
      ! calcualte the determinant and the inverse of the Jacobian matrix
      do kgauss = 1, ngauss
         call matrixCalc(dxdxi(:,:,kgauss),dxdxiInv(:,:,kgauss),
     +                     dxdxiDet(kgauss),mcrd)
         if(dxdxiDet(kgauss).le.(0.d0)) then
            write(*,*) 'Warning: negative Jacobian.'
         endif
      enddo   
      
      ! calculate the derivative of the shape function with respect to the 
      !  global coordinates
      do knode = 1, nnode
         do i = 1, mcrd
            do kgauss = 1, ngauss
               dshdx(knode,i,kgauss) = 0.d0
               do j = 1, mcrd
                  dshdx(knode,i,kgauss) = dshdx(knode,i,kgauss) + 
     +                    dxdxiInv(j,i,kgauss)*dshdxi(knode,j,kgauss)
               enddo
            enddo
         enddo
      enddo
      
      ! claculate the deformation gradient tensor
      do kgauss = 1, ngauss
         do i = 1, mcrd
            do j = 1,mcrd
               fInv(i,j,kgauss) = 0.d0
               do knode = 1, nnode
                  fInv(i,j,kgauss) = fInv(i,j,kgauss) + 
     +                             dshdx(knode,j,kgauss)*coords(i,knode)
               enddo
            enddo
         enddo
      enddo
      do kgauss = 1, ngauss
         call matrixCalc(fInv(:,:,kgauss),f(:,:,kgauss),
     +                     fInvDet(kgauss),mcrd)
         fDet(kgauss) = 1.d0/fInvDet(kgauss)
         if(fDet(kgauss).le.(0.d0)) then
            write(*,*) 'Warning: negative determinant for the
     + deformation gradient tensor.'
         endif
      enddo  
      
      return
      endsubroutine Local2Global
      
c---------------------------------------c
c                                       c
c          Utility subroutines          c
c                                       c
c---------------------------------------c
      subroutine matrixCalc(m,mInv,mDet,mcrd)
      implicit none
      ! input variables
      integer mcrd
      real*8 m(mcrd,mcrd)
      ! output variables
      real*8 mInv(mcrd,mcrd),mDet
      
      if(mcrd.eq.2) then
         mDet = m(1,1)*m(2,2)-m(1,2)*m(2,1)
         mInv(1,1) =  m(2,2)/mDet
         mInv(1,2) = -m(1,2)/mDet
         mInv(2,1) = -m(2,1)/mDet
         mInv(2,2) =  m(1,1)/mDet
      elseif(mcrd.eq.3) then
         mDet =  m(1,1)*m(2,2)*m(3,3) + m(1,2)*m(2,3)*m(3,1)
     +         + m(1,3)*m(2,1)*m(3,2) - m(1,3)*m(2,2)*m(3,1)
     +         - m(1,2)*m(2,1)*m(3,3) - m(1,1)*m(2,3)*m(3,2)
         mInv(1,1)= (m(2,2)*m(3,3)-m(2,3)*m(3,2))/mDet
         mInv(1,2)= (m(1,3)*m(3,2)-m(1,2)*m(3,3))/mDet
         mInv(1,3)= (m(1,2)*m(2,3)-m(1,3)*m(2,2))/mDet
         mInv(2,1)= (m(2,3)*m(3,1)-m(2,1)*m(3,3))/mDet
         mInv(2,2)= (m(1,1)*m(3,3)-m(1,3)*m(3,1))/mDet
         mInv(2,3)= (m(1,3)*m(2,1)-m(1,1)*m(2,3))/mDet
         mInv(3,1)= (m(2,1)*m(3,2)-m(2,2)*m(3,1))/mDet
         mInv(3,2)= (m(1,2)*m(3,1)-m(1,1)*m(3,2))/mDet
         mInv(3,3)= (m(1,1)*m(2,2)-m(1,2)*m(2,1))/mDet
      else
         !error: incorrect dimensions
         write(*,*) 'MCRD (dimensions) must be either 2 or 3.'
         call xit
      endif
      
      return
      endsubroutine matrixCalc

c---------------------------------------------------------------c
c                                                               c
c            UHYPER subroutine: Gent material model             c
c                                                               c
c---------------------------------------------------------------c
      subroutine uhyper(bi1,bi2,aj,u,ui1,ui2,ui3,temp,noel,
     1 cmname,incmpflag,numstatev,statev,numfieldv,fieldv,
     2 fieldvinc,numprops,props)

      implicit none
      ! variables that must be defined within the subroutine
      real*8 u(2),ui1(3),ui2(6),ui3(6),statev(*)
      ! variables that are passed to the subroutine for information
      character*80 cmname
      real*8 bi1,bi2,aj,temp,fieldv(*),fieldvinc(*),props(*)
      integer noel,incmpflag,numstatev,numfieldv,numprops
      
      ! variables that are used locally inside the subroutine
      real*8 mu,kapa,imax
      real*8 zero,half,one,two,three
      parameter(zero=0.d0,half=5.d-1,one=1.d0,two=2.d0,three=3.d0)
      
      ! get the material properties
      if(numprops.eq.3) then
         mu = props(1)
         kapa = props(2)
         imax = props(3)
      else
         print*, '***Error: the number of properties of the Gent
     + model in the uhyper subroutine must be 3.***'
         call xit
      endif 
      
      ! strain energy density function:
      ! deviatoric part of the strain energy density
      u(2) = -half*mu*imax*dlog(one-(bi1-three)/imax)
      ! total strain energy (deviatoric and volumetric)
      u(1) = u(2) + half*kapa*(aj-one)**two
      
      ! first derivatives of the strain energy with respect to
      !  the stretch invariants
      ! du/di1
      ui1(1) = half*mu/(one-(bi1-three)/imax)
      ! du/di2
      ui1(2) = zero
      ! du/di3
      ui1(3) = kapa*(aj-one)
      
      ! second derivatives of the strain energy
      ! d2u/d2i1
      ui2(1) = half*mu/imax/(one-(bi1-three)/imax)**two
      ! d2u/d2i2
      ui2(2) = zero
      ! d2u/d2i3
      ui2(3) = kapa
      ! d2u/di1di2
      ui2(4) = zero
      ! d2u/di1di3
      ui2(5) = zero
      ! d2u/di2di3
      ui2(6) = zero
      
      ! third derivatives of the strain energy
      ui3 = zero
      
      return
      end