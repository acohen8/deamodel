class CompletionInfo:
    def __init__(
        self,
        size
    ):
        self.completions = np.ones(size) # default to successful completions
        self.current_index = 0

def check_for_incomplete(job_name, message_type, data, completion_info):
    if ((message_type==ABORTED) or (message_type==ERROR)):
        completion_info.completions[completion_info.current_index] = 0
