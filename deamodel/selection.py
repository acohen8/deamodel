def group_cells(part, pts, set_name):
    """Select desired cells at given points and return a set containing them.

    Keyword arguments:
    part -- the part or part instance in which the set will be created
    pts -- a sequence of selection points (each of which is given as a tuple)
    set_name -- name of the set to be created
    """
    picked_geom = pick_cells(part, pts)
    return part.Set(cells=picked_geom, name=set_name)

def group_edges(part, pts, set_name):
    """Select desired faces at given points and return a set containing them.

    Keyword arguments:
    part -- the part or part instance in which the set will be created
    pts -- a sequence of selection points (each of which is given as a tuple)
    set_name -- name of the set to be created
    """
    picked_geom = pick_edges(part, pts)
    return part.Set(edges=picked_geom, name=set_name)

def group_faces(part, pts, set_name):
    """Select desired faces at given points and return a set containing them.

    Keyword arguments:
    part -- the part or part instance in which the set will be created
    pts -- a sequence of selection points (each of which is given as a tuple)
    set_name -- name of the set to be created
    """
    picked_geom = pick_faces(part, pts)
    return part.Set(faces=picked_geom, name=set_name)

def pick_cells(part, pts):
    picked_geom = []
    for pt in pts:
        picked_geom.append(part.cells.findAt((pt,),))
    return picked_geom

def pick_edges(part, pts):
    picked_geom = []
    for pt in pts:
        picked_geom.append(part.edges.findAt((pt,),))
    return picked_geom

def pick_faces(part, pts):
    picked_geom = []
    for pt in pts:
        picked_geom.append(part.faces.findAt((pt,),))
    return picked_geom
