# deamodel: A Toolbox for Modeling, Designing, and Analyzing Dielectric Elastomer Actuators
## Authorship
Primary owner and developer:<br/>
- Andy J. Cohen, Harvard Microrobotics Lab

Other key contributors:<br/>
- Ehsan Hajiesmaili, Clarke Group, Harvard (DEA material model implementation)
- Nikolas Vasio, Bertoldi Group, Harvard (Original linking guide for Abaqus)

## Purpose and overview
`deamodel` is meant to make it easy to model, design, and analyze dielectric elastomer actuators! DEAs hold incredible potential as artificial muscles for soft robots and devices, but they are notoriously difficult to model mathematically due to their coupled electro-mechanical nonlinearities. Thus FEA is an attractive solution for any DEA-based device beyond a simple membrane. FEAs have traditionally been conducted by hand-crafting simulations through a graphical interface without any parameterization, but this makes parameter studies for design and analysis excruciatingly slow and error-prone. Abaqus FEA, however, has a Python-based API that allows users to more easily create scripts that can be parameterized to run simulations. This package builds upon the Abaqus API to create DEA-specific functions such as slicing the part into electrode layers and automatically applying voltage there and defining the hyperelastic strain energy curves from commonly-measured mechanical characteristics.

## Collaboration
Collaboration plays a critical role in this process. DEA researchers and designers waste a lot of our time making our own scripts and material models, so this package is meant to provide templates, structures, and functions meant to reduce that overhead and, importantly, give us common tools and formats that can catalyze collaborative development in this field. I hope that other researchers will find this package useful, and will build projects based upon it. If you would like to contribute to the source code, please request developer access, and if you have a project you built off of it and are willing to share it, please contact me to place in the project directory. We would love to have as many examples as possible to help future users build their own DEA simulations.

## Project directory
The following are example projects created using `deamodel` showcasing its capabilities:<br/>
- [Simple DEA membrane](https://gitlab.com/acohen8/simple_dea_membrane)

## Getting started with `deamodel`
### Installing Abaqus with user subroutines
In order to run custom user subroutines - which our material model relies upon in the form of a user element (UEL) - you will need to install Abaqus on a computer and link it with a Fortran compiler. This can be done by following the steps in `abaqus-link.md` 
### Installing deamodel
Pull the git repo, and place it somewhere easy to access. For ease in adding it to the environment file in the next step, I would recommend placing it under your homepath somewhere (on Windows your homepath is your user folder). 

### Adding deamodel and your project to `$PYTHONPATH`
Abaqus sets up its own environment when it starts up, so in order to use any Python modules/packages, including 'deamodel' or your own custom packages, you will have to add them to the Python Path variable within this environment. This can be done by modifying the Abaqus environment file `abaqus.aev`, which sets up Abaqus's environment each time Abaqus is launched. Locate this file by searching within your `SIMULIA` folder. On my own machine, it was located at `C:\SIMULIA\CAE\2017\win_64\SMA\site`.

Open this file, and add deamodel to `$PYTHONPATH`. This allows Python to find deamodel when run from Abaqus's environment. Do this by adding the following lines to the file (preferably at the end for ease of reading):

```
# Set a variable for the path to deamodel on your machine, preferably under HOMEPATH somewhere (e.g. your_deamodel_path = $HOMEPATH/deamodel)
DEAMODEL_PATH your_deamodel_path

# This adds DEAMODELPATH to PYTHONPATH
PYTHONPATH $DEAMODEL_PATH:$PYTHONPATH
```

You can similarly add a path to your own packages by placing the following lines:
```
CUSTOM_PATH your_custom_path

PYTHONPATH $CUSTOM_PATH:$PYTHONPATH
```

### Running a simple membrane test
Clone the simple membrane test project linked above. After you've done so, open up the Abaqus Python Development Environment (PDE) and set the main file to be `tests/simple_test.py`. ADjust any of the geometric, material, or voltage parameters, and run it. The resulting strains and displacements will print out in the console. 
