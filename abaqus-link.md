With the following steps, which should be done in order, Abaqus can be correctly installed and configured to run the custom user subroutines that power deamodel's simulation capabilities. On my own machine, I use Abaqus 2017 with Visual Studio Community 2013 and Intel Parallel Studio 2017 Update 8, but you may use any compatible combination (compatibility can be determined through trial and error).

# 1. Install Visual Studio
Go to Microsoft and install Visual Studio Community. You may need to dig through the archives to find the 2013 version if you are using the same version as I am. You may need a free account to do this.

# 2. Install Intel Fortran compiler/Intel Parallel Studio 
Find Intel Parallel Studio XE on Intel's site and download it. 

When installing, make sure to check the box that says "Integrate with Visual Studio."

# 3. Install Abaqus
Install and enter your license information

# 4. Linking Abaqus, VS, and Fortran Compiler
Locate the following two files:<br/>
-`ifortvars.bat`: On my computer, it was at `C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2017.8.275\sindows\bin`
-`vcvars64.bat`: This may also be called `vcvarsamd64.bat` depending on your system and Abaqus version. On my computer, the location was C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin\amd64

Once you find these locations, add them to the Path environment variable on your computer. On Windows, environment variables can be accessed by going to Control Panel -> Advanced System Settings -> Environment Variables.

# 5. Invoking the Intel Fortran Compiler and Visual Studio when launching Abaqus
We need to make sure that the Intel Fortran Compiler and VS are launched along with Abaqus so that we can use them. To do so, we will modify the targets for both Abaqus Command (the command-line interface) and Abaqus CAE (the graphical interface). 

First locate the path to each of these applications on your machine. On my own machine, it was C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Dassault Systemes SIMULIA Abaqus CAE 2017 for both of them. 

## 5.a Abaqus Command 
Right click on Abaqus Command and click on properties. Change the existing "Target" from C:\windows\system32\cmd.exe /k to the following, with the quotation marks included:

"YOUR_IFORTVARS_PATH" intel64 vs2013 & YOUR_EXISTING_TARGET 

Note that if you visual studio version is different, change "vs2013" to match your version.

Save your changes.

## 5.b - Abaqus CAE
Similarly change the existing target to

"YOUR_IFORTVARS_PATH" intel64 vs2013 & YOUR_EXISTING_TARGET

, again being sure to change vs2013 if your version of VS is different. 

Save your changes.

## 6 - Verify correct installation
Open Abaqus Command and type `abaqus verify -all` to verify the setup of all Abaqus products, and type `abaqus verify -user_std` to verify the installation of Abaqus with user subroutines.
